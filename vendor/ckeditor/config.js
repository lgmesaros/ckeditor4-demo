/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'paragraph', groups: [ 'align', 'list', 'indent', 'blocks', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Save,Source,NewPage,Preview,Templates,PasteText,PasteFromWord,Copy,Paste,Cut,Styles,Find,Replace,Subscript,Superscript,SelectAll,CreateDiv,BidiLtr,Language,BidiRtl,Checkbox,Form,Radio,TextField,Textarea,Button,Select,HiddenField,Anchor,HorizontalRule,Flash,SpecialChar,Smiley,PageBreak,Iframe,About,ShowBlocks,Outdent,Indent';

	config.contentsCss = ['./css/editor.css'];

	config.extraPlugins = 'image2,widget,dialog,clipboard,font,colorbutton,panelbutton,floatpanel,save-to-pdf,print,videoembed';

	config.font_names = 'Roboto/Roboto, sans-serif;';

	config.font_names = 'Arial/Arial, Helvetica, sans-serif;Times New Roman/Times New Roman, Times, serif;' + config.font_names;

    config.font_defaultLabel = 'Arial';

    config.colorButton_colors = 'CCCCCC,66AB16';

    config.pdfHandler = './vendor/api2pdf/savetopdf.php'
};
